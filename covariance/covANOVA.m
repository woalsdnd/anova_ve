function K = covANOVA(hyp, x, z, projection_dim,index1,index2,edges)
% input : hyp = [ log(l_1)
%                 log(l_2)
%                    .
%                 log(l_D)
%                 log(sqrt(sf2)) ]
%
%         x (data points, each row is a data point)
%         z (data points, each row is a data point)
%         index1 (k^{index1,index2})
%         index2
%         (index1==0 && index2==0 -> full covariance (1+sum k^i+sum k^i*k^j), 
%          index1==1 && index2==0 -> k^1 
%          index1==1 && index2==2 -> k^{12}=k^1.*k^2
%          index1==-1 && index2==-1 -> original ANOVA) 
%
% return : covariance matrix of Kxz
%
% ANOVA covariance function based on Squared Exponential covariance function.
%
% k^i(zi,x'_i) = sf2 * (exp(-(zi - x'_i)^2/(2*l_i^2))
% 

%check dimensions
[n,D] = size(x); [n_,D_]=size(z); 
if ~D==D_ || D<projection_dim || (~index1==0 && index1==index2),return; end

%retrieve hyper parameters (length scales, signal)
l = exp(hyp(1:D));                               
sf2 = exp(2*hyp(D+1));                           

%calculate an ANOVA kernel matrix
if index1==0 && index2==0 %K_DD or K_Dx (kernel matrix for full anova)
    % K_anv(k,m,:)=exp(-(x(k,:)-z(m,:)).^2./(2*l'.^2))
    xz = -bsxfun(@minus,permute(x,[1 3 2]),permute(z,[3 1 2])).^2; 
    K_anv = exp(bsxfun(@rdivide,xz,permute(2*l'.^2,[1 3 2])));    
    
    if size(edges,1)~=0 % include second orders in edges
        second_order=arrayfun(@(i,j) bsxfun(@times,K_anv(:,:,i),K_anv(:,:,j)),edges(:,1),edges(:,2),'UniformOutput',false);
        K=ones(n,n_)+sum(K_anv,3)+sum(cat(3,second_order{:}),3);
    else % include all second order interaction
        %term1 : (sum k^i+1)^2+1 for all elements in nXn_ matrix
        %term2 :   sum (k^i)^2   for all elements in nXn_ matrix
        term1=bsxfun(@times,1+sum(K_anv,3),1+sum(K_anv,3))+1;
        term2=sum(bsxfun(@times,K_anv,K_anv),3);
        K=(term1-term2)/2;
    end
    
elseif index2==0 %K^index1(x,z)
    % K(k,m)=exp(-(x(k,index)-z(m,index))^2/(2*l(index1)^2))
    xz = -bsxfun(@minus,x(:,index1),permute(z(:,index1),[2 1])).^2; 
    K = exp(xz/(2*l(index1)^2));    
else %K^{index1,index2}(x,z)
    % K_anv(k,m,i)=exp(-(x(k,[index1 index2])-z(m,[index1 index2])).^2./(2*l'.^2))
    xz = -bsxfun(@minus,permute(x(:,[index1 index2]),[1 3 2]),permute(z(:,[index1 index2]),[3 1 2])).^2; 
    K_anv = exp(bsxfun(@rdivide,xz,permute(2*l([index1 index2])'.^2,[1 3 2])));    
    K = prod(K_anv,3);  
end

%multiply with signal
K=sf2*K;

end
function [X,Y,opt_pts,opt_values,model]=BayesianOptimization(model,plotIteration)
% INPUT : BO Model 
%
% OUTPUT : X,Y,optimal points, optimal values
%

for i=1:model.n_iterations
    %retrieve the next point
    next_x=retrieve_next(model);
    chosen_x=next_x;
    
    % if the same point is chosen, choose a random point
    if sum(ismember(model.X,next_x,'rows'))>0
        f_chosen_x=model.f(find(ismember(model.X,next_x,'rows')));
        while sum(ismember(model.X,next_x,'rows'))>0
            bound=model.bounds';
            next_x=unifrnd(bound(1,:),bound(2,:),1,length(next_x));
            f=model.obj_fun(next_x);
            warning('The same point is chosen. Draw a random point.');
        end
    else    %evalulate the chosen point
        f=model.obj_fun(next_x);
        f_chosen_x=f;
    end
    
    %plot current GP model, anova_dcop (debug), loglikelihood
    plotEachIteration(model,next_x,plotIteration);
       
    % augment new data and update kernel matrix, optimal value, optimizer
    model.X=[model.X;next_x];   model.f=[model.f;f];    model.n=model.n+1;
    model.history_chosen=[model.history_chosen;[chosen_x f_chosen_x]];
    model=updateGPmodel(model); 
    
    fprintf('current maximum : %d\n',model.max_val);
end

% set return values
X=model.history_chosen(:,1:end-1);  
Y=model.history_chosen(:,end);
opt_pts=model.history_opt(:,1:end-1);   
opt_values=model.history_opt(:,end);

end

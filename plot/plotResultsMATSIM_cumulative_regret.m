function plotResultsMATSIM_cumulative_regret(fun_name)
%   PLOT 
%   simple regret : f(x*)-max_xt f(xt)
%   average cumultative regret : sum^T {f(x*)-f(xt)}/T
%

%% set input, output path 
result_folder=[getenv('HOME_DIR') 'results/' fun_name '/'];
result_file=[result_folder 'result.mat'];

%% read data from files
load(result_file);

%% configure plot
leg={'original BO' ,'random embedding','subpsace learning','additive model','anova ve','random selection'}; % 5. legend
mark={'gv','rs','co','b*', 'md', 'rs','k+'}; 
color={'g','r','c','b', 'm', 'r','k'};
xtk=[0 50 100 150 200 250 300 350];
legend_interval=15;
n_iterations=length(simple_regret{1});
T=(0:n_iterations-1)';
mark_index=legend_interval:legend_interval:n_iterations;

%% plot average of cumulative regret
for method=[1,3,4,5,6,7]
    sr=reshape(cell2mat(avr_cumulative_regret(:,method)),n_iterations,[]);
    mean_sr=mean(sr,2);
    mean_sr_samples=mean_sr(mark_index);
    plot(mark_index-1,mean_sr_samples,mark{method});
    hold on;
end
legend(leg);

for method=[1,3,4,5,6,7]
    sr=reshape(cell2mat(avr_cumulative_regret(:,method)),n_iterations,[]);
    mean_sr=mean(sr,2);
    plot(T,mean_sr,color{method});
    mean_sr_samples=mean_sr(mark_index);
    std_sr=std(sr,0,2);
    std_sr_samples=std_sr(mark_index);
    errorbar(mark_index-1,mean_sr_samples,std_sr_samples/4,mark{method});
end
xlabel('Iterations');
ylabel('R_T/T');
xlim([0 350]);
set(gca,'XTick',xtk);
saveas(gcf,[result_folder fun_name '_avr_cumulative_regret.eps'],'epsc2');
close all;
clear;

end


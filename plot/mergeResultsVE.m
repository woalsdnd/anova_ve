function mergeResultsVE(no_experiment)
% set input, output path 
fun_name='logsumSE2';
result_folder=[getenv('HOME_DIR') 'results/' fun_name '/'];
file1=[result_folder 'result' int2str(no_experiment) '_others.mat'];
file2=[result_folder 'result' int2str(no_experiment) '_anovave.mat'];

f1=load(file1);
f2=load(file2);

simple_regret=f1.simple_regret;
avr_cumulative_regret=f1.avr_cumulative_regret;

for i=1:20
    simple_regret{i,6}=f2.simple_regret{i,5};
    avr_cumulative_regret{i,6}=f2.avr_cumulative_regret{i,5};
end

save([result_folder 'result' int2str(no_experiment) '.mat'],'simple_regret','avr_cumulative_regret');
end
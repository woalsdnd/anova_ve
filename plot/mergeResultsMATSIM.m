function mergeResultsMATSIM(fun_name)

    if strcmp(fun_name,'MATSIM_score')
        exp_num=1;
    elseif strcmp(fun_name,'MATSIM_travel_time')
        exp_num=2;
    end

    % set input, output path 
    result_folder=[getenv('HOME_DIR') 'results/' fun_name '/'];
    file1=[result_folder 'result' num2str(exp_num) '1.mat'];
    file2=[result_folder 'result' num2str(exp_num) '3.mat'];
    file3=[result_folder 'result' num2str(exp_num) '4.mat'];
    file4=[result_folder 'result' num2str(exp_num) '5.mat'];
    file5=[result_folder 'result_anova'];
    file6=[result_folder 'f_values_random'];
    
    f1=load(file1);
    f2=load(file2);
    f3=load(file3);
    f4=load(file4);
    f5=load(file5);
    f6=load(file6);
    
    simple_regret=f1.simple_regret;
    avr_cumulative_regret=f1.avr_cumulative_regret;

    simple_regret{1,3}=f2.simple_regret{1,3};
    simple_regret{1,4}=f3.simple_regret{1,4};
    simple_regret{1,5}=f4.simple_regret{1,5};
    simple_regret{1,6}=f5.simple_regret{1,5};
        
    avr_cumulative_regret{1,3}=f2.avr_cumulative_regret{1,3};
    avr_cumulative_regret{1,4}=f3.avr_cumulative_regret{1,4};
    avr_cumulative_regret{1,5}=f4.avr_cumulative_regret{1,5};
    avr_cumulative_regret{1,6}=f5.avr_cumulative_regret{1,5};
    
    %calculate average cumulative regret for random selection
    T=(1:length(f1.simple_regret{1,1}))';
    N=size(T,1);
    regret=-f6.f{1,1};
    avr_cumulative_regret{1,7}=zeros(N,1);
    for i=1:N
        avr_cumulative_regret{1,7}(i)=sum(regret(1:i))/T(i);
    end
  
    save([result_folder 'result.mat'],'simple_regret','avr_cumulative_regret');
end
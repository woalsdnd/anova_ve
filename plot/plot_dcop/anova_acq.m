function acq = anova_acq(model, xi,xj,i,j)
% return acquisition function (GP-UCB) for k^{ij}
% 
% xi is the value of ith variable
% xj is the value of jth variable
% in case of k^i, set xj=0, j=0  
% in case of full covariance, xi is the input point (a row vector)
%

if nargin==5
    x=zeros(1,model.high_dim);
    if (i>0),x(1,i)=xi;end    
    if (j>0),x(1,j)=xj;end
    [mu, var] = anova_mean_var(model, x,i,j);
elseif nargin==2
    [mu, var] = anova_mean_var(model, xi,0,0);
end

%compensate for signal in variance
var=var/(exp(model.hyp(end))*sqrt(model.high_dim*(model.high_dim+1)/2));

% approximate anova-UCB
coeff=anova_coeff(model);
acq=mu+sqrt(coeff)*var;
end

function result = variance_reduction(model,xi,xj,i,j,k,l,m,n)
% return variance reduction term
% formula : alpha*k^(kl)K^(-1)k^(mn) or alpha*k^(kl)K^(-1)1
%
% where
% xi : ith variable
% xj : jth variable
% k^(kl) : k^(kl)(x^(kl),x_D^(kl))
% K : K_DD or a data covariance matrix
% 1 : NX1 vector
% alpha : sqrt(coeff)/sf
% coeff : beta_t
% sf : signal
%
% if m==0 && n==0, alpha*k^(i)K^(-1)1
% otherwise, alpha*k^(ij)Kdd^(-1)k^(mn)
% 

if ~strcmp(model.kernel_type,'anova'),error('the model is not set to anova'); end

% set signal
sf=exp(model.hyp(end));

% set x based on xi,xj,i,j
x=zeros(1,model.high_dim);
if (i>0),x(1,i)=xi;end    
if (j>0),x(1,j)=xj;end

% set x based on xi,xj,m,n and calculate kmn_Nx
kkl_Nx = model.cov_model(model.hyp, model.X, x,k,l);
if m==0 && n==0 
    kmn_Nx = sf^2*ones(size(model.X,1),1);
else
    kmn_Nx = model.cov_model(model.hyp, model.X, x,m,n);
end

% calculate k^(kl)Kdd^(-1)k^(mn)
intermediate=model.L'\(model.L\kmn_Nx);
reduction_term=kkl_Nx'*intermediate;

% multiply beta and compensate for signal
delta=0.1;
D=model.high_dim;   t=model.n; coeff = 2*D*log(model.discretization*t/delta);
result=sqrt(coeff)*reduction_term/(sf*sqrt(model.high_dim*(model.high_dim+1)/2));

end


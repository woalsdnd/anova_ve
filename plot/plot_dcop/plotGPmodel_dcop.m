function plotGPmodel_dcop(model,n_ignore_pts,next_x,plotFullKernel)
%   INPUT : model,initial points to ignore,next point,
%           
%   FLAG : plotFullKernel(0 or 1)
%          
%% plot models for debug
    c=model.bounds(1, 2);   n_interval=model.discretization;
    [X,Y]=meshgrid(-c:c/n_interval:c,-c:c/n_interval:c);
    
%% calculate missing parts one by one
    k1_Nx = arrayfun(@(x,y)model.cov_model(model.hyp, model.X, [x,y],1,0),X,Y,'UniformOutput',false);
    k2_Nx = arrayfun(@(x,y)model.cov_model(model.hyp, model.X, [x,y],2,0),X,Y,'UniformOutput',false);
    k12_Nx = arrayfun(@(x,y)model.cov_model(model.hyp, model.X, [x,y],1,2),X,Y,'UniformOutput',false);
    
    sf = exp(model.hyp(end));
    [indices_Y,indices_X]=meshgrid(1:2*n_interval+1,1:2*n_interval+1);
    D=model.high_dim;   t=model.n; coeff = 20*(D+D^2)*log(2*t)/6;

%     var3=sqrt(coeff)/sf*arrayfun(@(x,y) k1_Nx{x,y}'*inv(model.L')*inv(model.L)*k2_Nx{x,y},indices_X,indices_Y);
%     var3_var_reduction=arrayfun(@(x,y) variance_reduction(model,x,y,1,0,2,0),X,Y);
%     var4=arrayfun(@(x,y) sqrt(coeff)*k1_Nx{x,y}'*inv(model.L')*inv(model.L)*k12_Nx{x,y},indices_X,indices_Y)/sf;
%     var4_var_reduction=arrayfun(@(x,y) variance_reduction(model,x,y,1,0,1,2),X,Y);
%     var5=arrayfun(@(x,y) sqrt(coeff)*(k2_Nx{x,y})'*inv(model.L')*inv(model.L)*k12_Nx{x,y},indices_X,indices_Y)/sf;
%     var5_var_reduction=arrayfun(@(x,y) variance_reduction(model,x,y,2,0,1,2),X,Y);
%     var6=arrayfun(@(x,y) sqrt(coeff)*sf*(k1_Nx{x,y})'*inv(model.L')*inv(model.L)*ones(model.n,1),indices_X,indices_Y);
%     var6_var_reduction=arrayfun(@(x,y) variance_reduction(model,x,y,1,0,0,0),X,Y);
    var7=arrayfun(@(x,y) sqrt(coeff)*sf*(k2_Nx{x,y})'*inv(model.L')*inv(model.L)*ones(model.n,1),indices_X,indices_Y);
    var7_var_reduction=arrayfun(@(x,y) variance_reduction(model,x,y,2,0,0,0),X,Y);
%     var8=arrayfun(@(x,y) sqrt(coeff)*sf*(k12_Nx{x,y})'*inv(model.L')*inv(model.L)*ones(model.n,1),indices_X,indices_Y);
%     var8_var_reduction=arrayfun(@(x,y) variance_reduction(model,x,y,1,2,0,0),X,Y);
  
%     figure(7);clf('reset');
%     var_including_missing_part=constant+var1+var2+var12-2*(var3+var4+var5+var6+var7+var8);
%     pcolor(X,Y,var_including_missing_part); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
%     title('var including missing parts');



%     figure;
%     pcolor(X,Y,var3); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
%     saveas(gcf, [getenv('HOME_DIR') 'image/' num2str(model.n) 'var3.jpg']);
%     close all;
%     figure;
%     pcolor(X,Y,var3_var_reduction); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
%     saveas(gcf, [getenv('HOME_DIR') 'image/' num2str(model.n) 'var3_var_reduction.jpg']);
%     figure;
%     pcolor(X,Y,var3); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
%     saveas(gcf, [getenv('HOME_DIR') 'image/' num2str(model.n) 'var3.jpg']);
%     close all;
%     figure;
%     pcolor(X,Y,var3_var_reduction); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
%     saveas(gcf, [getenv('HOME_DIR') 'image/' num2str(model.n) 'var3_var_reduction.jpg']);
%     figure;
%     pcolor(X,Y,var3); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
%     saveas(gcf, [getenv('HOME_DIR') 'image/' num2str(model.n) 'var3.jpg']);
%     close all;
%     figure;
%     pcolor(X,Y,var3_var_reduction); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
%     saveas(gcf, [getenv('HOME_DIR') 'image/' num2str(model.n) 'var3_var_reduction.jpg']);
%     figure;
%     pcolor(X,Y,var4); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
%     saveas(gcf, [getenv('HOME_DIR') 'image/' num2str(model.n) 'var4.jpg']);
%     close all;
%     figure;
%     pcolor(X,Y,var4_var_reduction); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
%     saveas(gcf, [getenv('HOME_DIR') 'image/' num2str(model.n) 'var4_var_reduction.jpg']);
%     figure;
%     pcolor(X,Y,var5); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
%     saveas(gcf, [getenv('HOME_DIR') 'image/' num2str(model.n) 'var5.jpg']);
%     close all;
%     figure;
%     pcolor(X,Y,var5_var_reduction); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
%     saveas(gcf, [getenv('HOME_DIR') 'image/' num2str(model.n) 'var5_var_reduction.jpg']);
%     figure;
%     pcolor(X,Y,var6); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
%     saveas(gcf, [getenv('HOME_DIR') 'image/' num2str(model.n) 'var6.jpg']);
%     close all;
%     figure;
%     pcolor(X,Y,var6_var_reduction); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
%     saveas(gcf, [getenv('HOME_DIR') 'image/' num2str(model.n) 'var6_var_reduction.jpg']);
    
    figure;
    pcolor(X,Y,var7); shading flat;colorbar;%caxis([0,5]);
    hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
    saveas(gcf, [getenv('HOME_DIR') 'image/' num2str(model.n) 'var7.jpg']);
    close all;
    figure;
    pcolor(X,Y,var7_var_reduction); shading flat;colorbar;%caxis([0,5]);
    hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
    saveas(gcf, [getenv('HOME_DIR') 'image/' num2str(model.n) 'var7_var_reduction.jpg']);
    figure;
%     pcolor(X,Y,var8); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
%     saveas(gcf, [getenv('HOME_DIR') 'image/' num2str(model.n) 'var8.jpg']);
%     close all;
%     figure;
%     pcolor(X,Y,var8_var_reduction); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
%     saveas(gcf, [getenv('HOME_DIR') 'image/' num2str(model.n) 'var8_var_reduction.jpg']);
%     
    
%%    
    
    
    [mean,var]=arrayfun(@(x,y) anova_mean_var(model,[x,y],0,0), X,Y);
    acq1=arrayfun(@(x,y) anova_acq(model,x,y,1,0), X,Y);
    acq2=arrayfun(@(x,y) anova_acq(model,y,x,2,0), X,Y);
    acq12=arrayfun(@(x,y) anova_acq(model,x,y,1,2), X,Y);

    if plotFullKernel
        acq=arrayfun(@(x,y) anova_acq(model,[x,y],0,0), X,Y);
    
        f101=figure(101);
        pcolor(X,Y,var); shading flat;colorbar;%caxis([0,5]);
        hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');
        %scatter(next_x(:,1),next_x(:,2),30,'w','filled');
        %scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
        saveas(f101, [getenv('HOME_DIR') 'image/' num2str(model.n) 'full_variance.jpg']);
    
        f103=figure(103);clf('reset');
        pcolor(X,Y,acq); shading flat;colorbar;%caxis([0,5]);
        hold on;
        scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');
        %scatter(next_x(:,1),next_x(:,2),30,'w','filled');
        %scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
        saveas(f103, [getenv('HOME_DIR') 'image/' num2str(model.n) 'full_ucb.jpg']);
    end
    
    %% plot approximated ucb and posterior mean
    f=figure(106);clf('reset'); %subplot(1,2,1);
    %figure(106);
    approx_ucb=acq1+acq2+acq12; [m,index]=max(approx_ucb(:));
    pcolor(X,Y,approx_ucb); shading flat;colorbar;%caxis([8.49,8.51]);
    hold on;
    scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');
    scatter(next_x(:,1),next_x(:,2),30,'w','filled');
    scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled');
    scatter(X(index),Y(index),30,'r','filled');
    title('approximated UCB'); 
    %     saveas(f, [getenv('HOME_DIR') 'image/' num2str(model.n) 'approximated_ucb.jpg']);
    
    %subplot(1,2,2);
    f2=figure(107);clf('reset');
    pcolor(X,Y,mean); shading flat;colorbar;%caxis([-50,0]);
    hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');
    scatter(next_x(:,1),next_x(:,2),30,'w','filled');
    scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled');
    scatter(X(index),Y(index),30,'r','filled');
    title('posterior mean without constant'); 
    %     saveas(f2, [getenv('HOME_DIR') 'image/' num2str(model.n) 'mean_without_constant.jpg']);
   
%% calculate mean1,var1, mean2,var2, mean3,var3
% [mean1,var1]=arrayfun(@(x,y) anova_mean_var(model,[x,y],1,0), X,Y);
% [mean12,var12]=arrayfun(@(x,y) anova_mean_var(model,[x,y],1,2), X,Y);
% [mean2,var2]=arrayfun(@(x,y) anova_mean_var(model,[x,y],2,0), X,Y);

%% calculate missing parts one by one
%     k1_Nx = arrayfun(@(x,y)model.cov_model(model.hyp, model.X, [x,y],1,0),X,Y,'UniformOutput',false);
%     k2_Nx = arrayfun(@(x,y)model.cov_model(model.hyp, model.X, [x,y],2,0),X,Y,'UniformOutput',false);
%     k12_Nx = arrayfun(@(x,y)model.cov_model(model.hyp, model.X, [x,y],1,2),X,Y,'UniformOutput',false);
% 
%     [indices_Y,indices_X]=meshgrid(1:2*n_interval+1,1:2*n_interval+1);
%     
%     var3=arrayfun(@(x,y) (k1_Nx{x,y})'*model.L'\(model.L\k2_Nx{x,y}),indices_X,indices_Y);
%     var4=arrayfun(@(x,y) (k1_Nx{x,y})'*model.L'\(model.L\k12_Nx{x,y}),indices_X,indices_Y);
%     var5=arrayfun(@(x,y) (k2_Nx{x,y})'*model.L'\(model.L\k12_Nx{x,y}),indices_X,indices_Y);
%     var6=arrayfun(@(x,y) sf2*(k1_Nx{x,y})'*model.L'\(model.L\ones(model.n,1)),indices_X,indices_Y);
%     var7=arrayfun(@(x,y) sf2*(k2_Nx{x,y})'*model.L'\(model.L\ones(model.n,1)),indices_X,indices_Y);
%     var8=arrayfun(@(x,y) sf2*(k12_Nx{x,y})'*model.L'\(model.L\ones(model.n,1),)indices_X,indices_Y);
% 
%     constant=sf2-sf2^2*ones(1,model.n)*model.L'\(model.L\ones(model.n,1));
% 
%     figure(7);clf('reset');
%     var_including_missing_part=constant+var1+var2+var12-2*(var3+var4+var5+var6+var7+var8);
%     pcolor(X,Y,var_including_missing_part); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
%     title('var including missing parts');

%% plot variance partially adding missing parts 
%     figure(15);clf('reset');
%     pcolor(X,Y,log(1+const+var1+var2+var12-2*(var6+var7+var8))); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled'); title('approximated var including 678');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled');
% 
%     figure(21);clf('reset');
%     pcolor(X,Y,log(1+const+var1+var2+var12-2*(var3+var4+var5))); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled'); title('approximated var including 345');
%     
%% plot var1, var2, var12
% figure(10);clf('reset');
% pcolor(X,Y,sqrt(var1)); shading flat;colorbar;%caxis([0,5]);
% hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled'); title('var 1');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled');
% 
% figure(11);clf('reset');
% pcolor(X,Y,sqrt(var2)); shading flat;colorbar;%caxis([0,5]);
% hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled'); title('var 2');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled');
% 
% figure(12);clf('reset');
% pcolor(X,Y,sqrt(var12)); shading flat;colorbar;%caxis([0,5]);
% hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled'); title('var 12');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled');
%% plot missing variance            
% figure(102);clf('reset');
% pcolor(X,Y,var-var1-var2-var12); shading flat;colorbar;%caxis([0,5]);
% hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');
% scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled'); 
% title('var missing parts');
%% plot approximated variance and sum of sqrt of variance
%     const=sf2-sf2^2*ones(1,model.n)*model.L'\(model.L\ones(model.n,1));
%     figure(107);clf('reset');
%     pcolor(X,Y,const+var1+var2+var12); shading flat;colorbar;%caxis([0,0.14]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled');
%     title('approximated var');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled');
%     
%     figure(108);clf('reset');
%     pcolor(X,Y,sqrt(var12)+sqrt(var1)+sqrt(var2)); shading flat;colorbar;%caxis([0,5]);
%     hold on;scatter(model.X(n_ignore_pts+1:end,1),model.X(n_ignore_pts+1:end,2),30,'k','filled');scatter(next_x(:,1),next_x(:,2),30,'w','filled'); 
%     title('sum sqrt approximated vars');scatter(model.max_x(:,1),model.max_x(:,2),30,'b','filled');
%     
end

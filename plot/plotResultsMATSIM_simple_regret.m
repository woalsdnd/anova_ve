function plotResultsMATSIM_simple_regret(fun_name)
%   PLOT 
%   simple regret : f(x*)-max_xt f(xt)
%   average cumultative regret : sum^T {f(x*)-f(xt)}/T
%

%% set input, output path 
result_folder=[getenv('HOME_DIR') 'results/' fun_name '/'];
result_file=[result_folder 'f.mat'];

%% read data from files
load(result_file);

%retrieve optimum value of the function
optimum=0;

%calculate simple regret and average cumulative regret
run=1;
T=(0:length(f{1,1})-1)';
N=size(T,1);
for method=[1,3,4,5,6,7,8]
    regret=optimum-f{run,method};
    for i=1:N
        simple_regret{run,method}(i)=min(regret(1:i));
    end
end
%% configure plot
leg={'original BO','random embedding','subpsace learning','additive model','anova ve','genetic algorithm','random selection'}; % 5. legend
mark={'gv', 'k+','co','b*', 'md','rs','y^','k+'}; 
color={'g','k','c','b', 'm', 'r','y','k'};
xtk=[0 50 100 150 200 250 300 350 400 450];
legend_interval=15;
n_iterations=length(simple_regret{1});
T=(0:n_iterations-1)';
mark_index=[legend_interval:legend_interval:n_iterations,399];

%% plot simple regret
for method=[1,3,4,5,6,7,8]
    sr=reshape(cell2mat(simple_regret(:,method)),n_iterations,[]);
    mean_sr=mean(sr,2);
    mean_sr_samples=mean_sr(mark_index);
    plot(mark_index-1,mean_sr_samples,mark{method});
    hold on;
end
legend(leg);

for method=[1,3,4,5,6,7,8]
    sr=reshape(cell2mat(simple_regret(:,method)),n_iterations,[]);
    mean_sr=mean(sr,2);
    plot(T,mean_sr,color{method});
    mean_sr_samples=mean_sr(mark_index);
    std_sr=std(sr,0,2);
    std_sr_samples=std_sr(mark_index);
    errorbar(mark_index-1,mean_sr_samples,std_sr_samples/4,mark{method});
end

xlabel('Number of evaluations');
if strcmp(fun_name,'MATSIM_score')
    ylabel('Simple Regret (penalty)');
elseif strcmp(fun_name,'MATSIM_travel_time')
    ylabel('Simple Regret (travel time)');
end
xlim([0 450]);
set(gca,'XTick',xtk);
saveas(gcf,[result_folder fun_name '_f_simple_regret.eps'],'epsc2');
close all;

end


function mergeResultsMATSIM_f(fun_name)

    if strcmp(fun_name,'MATSIM_score')
        exp_num=1;
    elseif strcmp(fun_name,'MATSIM_travel_time')
        exp_num=2;
    end

    % set input, output path 
    result_folder=[getenv('HOME_DIR') 'results/' fun_name '/'];
    file1=[result_folder 'f_values' num2str(exp_num) '1.mat'];
    file2=[result_folder 'f_values' num2str(exp_num) '3.mat'];
    file3=[result_folder 'f_values' num2str(exp_num) '4.mat'];
    file4=[result_folder 'f_values' num2str(exp_num) '5.mat'];
    file5=[result_folder 'f_values_anova.mat'];
    file6=[result_folder 'ga.mat'];
    file7=[result_folder 'f_values_random'];
    
    f1=load(file1);
    f2=load(file2);
    f3=load(file3);
    f4=load(file4);
    f5=load(file5);
    f6=load(file6);
    f7=load(file7);
    
    f=f1.f;
    f{1,3}=f2.f{1,3};
    f{1,4}=f3.f{1,4};
    f{1,5}=f4.f{1,5};
    f{1,6}=f5.f{1,5};
    if strcmp(fun_name,'MATSIM_score')
        f{1,7}=f6.scores;
    elseif strcmp(fun_name,'MATSIM_travel_time')
        f{1,7}=f6.tt;
    end
    f{1,8}=f7.f{1,1};
    
    save([result_folder 'f.mat'],'f');
end
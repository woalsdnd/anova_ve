function plotLogLikelihood(model)
% INPUT : model, objective function, figure number
%
    s=model.hyper_bound(1, 1);    e=model.hyper_bound(1, 2);
    n_interval=50;
    [X,Y]=meshgrid(s:1/n_interval:e,s:1/n_interval:e);
   
    log_lik=arrayfun(@(x,y) (log_likelihood([x; y; model.hyp(end)], model)), X,Y);
            
	figure;
    pcolor(X,Y,log_lik);
    hold on;    shading flat;   colorbar;%caxis([-50,0]);
	[m,index]=max(log_lik(:));  scatter(X(index),Y(index),30,'k','filled');
    saveas(gcf,[getenv('HOME_DIR') 'image/' num2str(model.n) '_log_liklihood_original_bo.eps'],'epsc2');
    close all;
    

end


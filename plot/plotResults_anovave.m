function plotResults_anovave(fun_name,no_experiment)
%   PLOT 
%   simple regret : f(x*)-max_xt f(xt)
%   average cumultative regret : sum^T {f(x*)-f(xt)}/T
%

%% set input, output path 
result_folder=[getenv('HOME_DIR') 'results/' fun_name '/'];
result_file=[result_folder 'result' int2str(no_experiment) '.mat'];

%% read data from files
load(result_file);

%% configure plot
leg={'original BO' 'random basis','random embedding','subpsace learning','additive model','anova ve'}; % 5. legend
mark={'gv','rs','co','b*', 'md', 'k+'}; 
color={'g','r','c','b', 'm', 'k'};
xtk=[0 50 100 150 200 250 300 350 400 450 500];
legend_interval=20;
n_iterations=length(simple_regret{1});
T=(0:n_iterations-1)';
mark_index=legend_interval:legend_interval:n_iterations;

%% plot simple regret
for method=1:6
    sr=reshape(cell2mat(simple_regret(:,method)),n_iterations,[]);
    mean_sr=mean(sr,2);
    mean_sr_samples=mean_sr(mark_index);
    semilogy(mark_index-1,mean_sr_samples,mark{method});
    hold on;
end
legend(leg{1:end});

for method=1:6
    sr=reshape(cell2mat(simple_regret(:,method)),n_iterations,[]);
    mean_sr=mean(sr,2);
    semilogy(T,mean_sr,color{method});
    mean_sr_samples=mean_sr(mark_index);
    std_sr=std(sr,0,2);
    std_sr_samples=std_sr(mark_index);
    errorbar(mark_index-1,mean_sr_samples,std_sr_samples/4,mark{method});
end

xlabel('Iterations');
ylabel('Simple Regret');
xlim([0 550]);
set(gca,'XTick',xtk);
saveas(gcf,[result_folder fun_name '_simple_regret_' int2str(no_experiment) '.eps'],'epsc2');
close all;

%% plot average of cumulative regret
for method=1:6
    sr=reshape(cell2mat(avr_cumulative_regret(:,method)),n_iterations,[]);
    mean_sr=mean(sr,2);
    mean_sr_samples=mean_sr(mark_index);
    semilogy(mark_index-1,mean_sr_samples,mark{method});
    hold on;
end
legend(leg{1:end});

for method=1:6
    sr=reshape(cell2mat(avr_cumulative_regret(:,method)),n_iterations,[]);
    mean_sr=mean(sr,2);
    semilogy(T,mean_sr,color{method});
    mean_sr_samples=mean_sr(mark_index);
    std_sr=std(sr,0,2);
    std_sr_samples=std_sr(mark_index);
    errorbar(mark_index-1,mean_sr_samples,std_sr_samples/4,mark{method});
end
xlabel('Iterations');
ylabel('R_T/T');
xlim([0 550]);
set(gca,'XTick',xtk);
saveas(gcf,[result_folder fun_name '_avr_cumulative_regret_' int2str(no_experiment) '.eps'],'epsc2');
close all;
clear;

end


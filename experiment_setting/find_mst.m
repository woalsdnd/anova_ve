function model=find_mst(method,original_obj_fun,n_init_pts,n_iterations,high_dim,projection_dim,hyp_update_cycle)
% Initialize GP model specifying the followings : 
% #initial points, #points, input points, function values, 
% #iterations (in the BO loop), 
% original dimensions, projection dimensions, 
% kernel type, covariance function,
% (box constrained) domain, 
% hyperparamters, range of hyperparameters, 
% prior mean,
% discretization density, 
% objective function,
% lower cholesky matrix, 
% history (in optimization), current max, current max point.
% cycle of updating hyperparameters
%

%% function-specific parameters
% set fixed seed for debug
sec=datevec(now); 
seed = 1000*sec(6); randn('seed',seed), rand('seed',seed);

% limit the initial points
bounds_inits=[0,1];

% set prior mean
model.prior_mean=0;

% set discretization level for anova_dcop (in the domain of [-1,1]^*)
model.discretization=20;

% set a range of hyperparameters (lengthscale, sigma_f, sigma_n)
if isequal(original_obj_fun,@MATSIM_score)
    hrange.lengthscale=[6,6];
elseif isequal(original_obj_fun,@MATSIM_travel_time)
    hrange.lengthscale=[14,14];
end    
hrange.signal=[0.1,0.1]; %fix signal and noise (rule of thumb)
hrange.noise=[0.01,0.01];
%rescale the range of signal to have identical value in the full kernels 
if method==4 %additive
    hrange.signal=hrange.signal/sqrt(high_dim);
elseif method==5 %anova
    hrange.signal=hrange.signal/sqrt(2*high_dim);
end

%% set objection function, initial points and corresponding function values
[model.obj_fun,model.X,model.f,bounds_domain]=load_initial_setting(method,n_init_pts,bounds_inits,high_dim,projection_dim,original_obj_fun);

%% set model parameters
model=set_model_parameters(model,method,hrange,n_init_pts,n_iterations,high_dim,projection_dim,bounds_domain,hyp_update_cycle);

%% set the file name of MST
if isequal(original_obj_fun,@MATSIM_score)
    mst_file_name=[getenv('HOME_DIR') 'mst_p.mat'];
elseif isequal(original_obj_fun,@MATSIM_score_expressways)
    mst_file_name=[getenv('HOME_DIR') 'mst_p_expressways.mat'];
elseif isequal(original_obj_fun,@MATSIM_travel_time)
    mst_file_name=[getenv('HOME_DIR') 'mst_tt.mat'];
elseif isequal(original_obj_fun,@MATSIM_travel_time_expressways)
    mst_file_name=[getenv('HOME_DIR') 'mst_tt_expressways.mat'];
end
        
%% build MST
tic;
if model.high_dim~=2 && strcmp(model.kernel_type,'anova')
    [edges,mst]=buildMaximumSpanningTree(model);  
    save(mst_file_name,'edges','mst');
end
fprintf('Building spanning tree : %d sec\n',toc);

end

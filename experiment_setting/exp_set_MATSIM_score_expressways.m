function model=exp_set_MATSIM_score_expressways(method,original_obj_fun,n_init_pts,n_iterations,high_dim,projection_dim,hyp_update_cycle)
% Initialize GP model specifying the followings : 
% #initial points, #points, input points, function values, 
% #iterations (in the BO loop), 
% original dimensions, projection dimensions, 
% kernel type, covariance function,
% (box constrained) domain, 
% hyperparamters, range of hyperparameters, 
% prior mean,
% discretization density, 
% objective function,
% lower cholesky matrix, 
% history (in optimization), current max, current max point.
% cycle of updating hyperparameters
%

%% function-specific parameters
% set fixed seed for debug
sec=datevec(now); 
seed = 1000*sec(6); randn('seed',seed), rand('seed',seed);

% limit the initial points
bounds_inits=[-inf,0];

% set prior mean
model.prior_mean=0;

% set discretization level for anova_dcop (in the domain of [-1,1]^*)
model.discretization=20;

% set a range of hyperparameters (lengthscale, sigma_f, sigma_n)
hrange.lengthscale=[0.1,2];
hrange.signal=[0.2,0.2]; %fix signal and noise (rule of thumb)
hrange.noise=[0.02,0.02];
%rescale the range of signal to have identical value in the full kernels 
hrange.signal=hrange.signal/sqrt(2*high_dim);

%% set objective function, initial points and corresponding function values
[model.obj_fun,model.X,model.f,bounds_domain]=set_initial_setting(method,n_init_pts,bounds_inits,high_dim,projection_dim,original_obj_fun);

%% set model parameters
model=set_model_parameters(model,method,hrange,n_init_pts,n_iterations,high_dim,projection_dim,bounds_domain,hyp_update_cycle);

%% set the file name of MST
mst_file_name=[getenv('HOME_DIR') 'mst_p_expressways.mat'];

%% load MST
load(mst_file_name)
model.edges=edges;
model.mst=mst;

%% update Cholesky matrix, optimal value, optimizer, history
model=updateGPmodel(model);

end

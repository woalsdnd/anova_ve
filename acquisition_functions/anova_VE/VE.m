function next_x=VE(model)
%% build the Parent-Children table
pc_table{model.high_dim}=[];    Depth{model.high_dim}=[];
pc_table{1}={[],[]};    depth=0;    next_group=1;   tree=model.mst;
while ~isempty(next_group)
    curr_group=next_group;  next_group=[];  
    depth=depth+1;  Depth{depth}=curr_group;
    for i=1:length(curr_group)
        curr_node=curr_group(i);
        %get indices of connected nodes
        col=sum(tree(curr_node,:)>0,1);    col_index=find(col);
        row=sum(tree(:,curr_node)>0,2)';   row_index=find(row);
        %register children to its table
        pc_table{curr_node}{2}=[row_index col_index];
        %add children to the next group
        next_group=[next_group row_index col_index];
        %set parent node to its children node
        for j=1:length(next_group), pc_table{next_group(j)}{1}=curr_node;end
        %clear the edges connected to the current node
        tree(curr_node,col_index)=0;   tree(row_index,curr_node)=0;
    end
end
max_depth=depth;
% view(biograph(model.mst,[],'ShowArrows','off','ShowWeights','on'))

%% send functions to the root node
% set miscellaneous variables
D=model.high_dim;   coeff=anova_coeff(model);   CHI=length(model.X_discrete);   sf2=exp(2*model.hyp(end));
[INDEX1,INDEX2]=meshgrid(1:1:length(model.X_discrete),1:1:length(model.X_discrete));
index_table{D}=[];  f_to_p{D}=[];
% propagate function messages to the top & build a max-index table
for depth=max_depth:-1:1
    curr_group=Depth{depth};
    for i_group=1:length(curr_group)
        curr_node=curr_group(i_group);    
        parent=pc_table{curr_node}{1};  
        children=pc_table{curr_node}{2};
        f=zeros(1,CHI);
        %add functions from children
        for m=1:length(children),   f=f+f_to_p{children(m)};    end
        %add function of its own variable
        f=f+arrayfun(@(index) model.prior_mean+model.k_iD{curr_node}{index}'*model.K_inv_f+sqrt(coeff)*(model.k_i{curr_node}(index)-model.k_iVARreduction{curr_node}(index)), 1:length(model.X_discrete));
        %add a conjunctive function with the parent
        if depth==1
            [max_f,index_table{curr_node}]=max(f);
        else
            [f,index_table{curr_node}]=max(repmat(f,CHI,1)+arrayfun(@(index1,index2) interaction_term_dcop(model,model.k_i{curr_node}(index1),model.k_i{parent}(index2),model.k_iD{curr_node}{index1},model.k_iD{parent}{index2},model.k_iVARreduction{curr_node}(index1),model.k_iVARreduction{parent}(index2)), INDEX1,INDEX2),[],2);
            f_to_p{curr_node}=f';
        end
    end
end

%% get the index of optimal points
for depth=2:max_depth
    curr_group=Depth{depth};
    for i=1:length(curr_group)
        curr_node=curr_group(i);    
        parent=pc_table{curr_node}{1};  
        index_table{curr_node}=index_table{curr_node}(index_table{parent});
    end
end  

%% return the optimal point
next_x=zeros(1,D);
for i=1:D,  next_x(i)=(index_table{i}-1)/model.discretization-1;  end

end

function [edges,mst]=buildMaximumSpanningTree(model)

% learn hyper parameters allowing all interactions
% model.hyp=learn_hyperparameters(model);

% get lower cholesky of data covariance matrix with all interaction
cov = model.cov_model(model.hyp,model.X,model.X, 0,0,[]) + exp(2*model.noise)*eye(model.n);
L = chol(cov, 'lower');

% calculate kernel functions and K^{-1}(f-mu) in anova-dcop
tic;
model.K_inv_f=L'\(L\(model.f-model.prior_mean));
fprintf('Calculating data kernel matrices for all second order interaction: %d sec\n',toc);

% build a factor graph with weights
D=model.high_dim;   d=model.projection_dim;%projection_dim should be 2 
comb = nchoosek(1:D,d);
weights=arrayfun(@(i,j) weight_edge(model,i,j),comb(:,1),comb(:,2));

% flip the weight (max weight into min weight and vice versa)
max_w=max(weights(:));
complementary_w=max_w-weights;

% find a minimum spanning tree (lower triangular matrix)
[w_st,mst_neighbor,mst_adj]=kruskal(comb,complementary_w);
mst=tril(mst_adj,-1);
edges=mst_neighbor

% view(biograph(mst,[],'ShowArrows','off','ShowWeights','on'))

end
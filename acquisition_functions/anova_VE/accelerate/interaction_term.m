function result = interaction_term(model,xi,xj,i,j)
% return interaction terms between ith variable and jth variable
% in final approximate anova-UCB 
% 
% INPUT : xi (ith value), xj (jth value)
%
% OUTPUT : a joint function between ith and jth variables
% 

if ~strcmp(model.kernel_type,'anova'),error('the model is not set to anova'); end

% sqaure of signal, original dimension
sf2=exp(2*model.hyp(end));

% calculate k^(i)(x^(i),X^(i)), k^(j)(x^(j),X^(j))
k_Nx_i = anova_cov_matrix(model,xi,0,i,0,[],'cross');
k_Nx_j = anova_cov_matrix(model,xj,0,j,0,[],'cross');

% calculate k^(ij)(x^(ij),X^(ij))
% and compensate for the variance
k_Nx_ij=k_Nx_i.*k_Nx_j/sf2;

% calculate posterior mean k^(ij)(x^(ij),X^(ij))*K^{-1}*(y-mu)
post_mean=model.prior_mean+ k_Nx_ij'*model.K_inv_f;

result=post_mean;

end


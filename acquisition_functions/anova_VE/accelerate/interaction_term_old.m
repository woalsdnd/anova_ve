function result = interaction_term_old(model,xi,xj,i,j)
% return interaction terms between ith variable and jth variable
% in final approximate anova-UCB 
% 
% INPUT : xi (ith value), xj (jth value)
%
% OUTPUT : a joint function between ith and jth variables
% 

if ~strcmp(model.kernel_type,'anova'),error('the model is not set to anova'); end

% sqaure of signal, original dimension
sf2=exp(2*model.hyp(end));
D=model.high_dim;

% calculate k^(i)(x^(i),x^(i)), k^(i)(x^(i),X^(i)),
%           k^(j)(x^(j),x^(j)), k^(j)(x^(j),X^(j))
k_xx_i=anova_cov_matrix(model,xi,0,i,0,[],'auto');
k_xx_j=anova_cov_matrix(model,xj,0,j,0,[],'auto');
k_Nx_i = anova_cov_matrix(model,xi,0,i,0,[],'cross');
k_Nx_j = anova_cov_matrix(model,xj,0,j,0,[],'cross');

% calculate k^(ij)(x^(ij),x^(ij)), k^(ij)(x^(ij),X^(ij))
% and compensate for the variance
k_xx_ij=k_xx_i.*k_xx_j/sf2;                           
k_Nx_ij=k_Nx_i.*k_Nx_j/sf2;

% calculate posterior mean k^(ij)(x^(ij),X^(ij))*K^{-1}*(y-mu)
post_mean=model.prior_mean+ k_Nx_ij'*model.K_inv_f;

% calculate vairance terms
% term1'=1+k^(i)(x^(i),X^(i))+k^(j)(x^(j),X^(j))+k^(ij)(x^(ij),X^(ij))
% term2=term1'*K^{-1}*term1
% term3=k^(i)(x^(i),X^(i))*K^{-1}*k^(i)(x^(i),X^(i))'+2*k^(i)(x^(i),X^(i))*K^{-1}*1
% term4=k^(j)(x^(j),X^(j))*K^{-1}*k^(j)(x^(j),X^(j))'+2*k^(j)(x^(j),X^(j))*K^{-1}*1
term1=1+k_Nx_i+k_Nx_j+k_Nx_ij;
intermediate2=model.L'\(model.L\term1);
term2=term1'*intermediate2;
term3=anova_var_reduction(model,k_Nx_i);
term4=anova_var_reduction(model,k_Nx_j);

% collect variance
var=k_xx_ij-term2+term3+term4;

% multiply beta and compensate for signal
coeff=anova_coeff(model);
result=post_mean+sqrt(coeff)*var/sqrt(sf2*D*(D+1)/2);

end


function cov_matrix = anova_cov_matrix(model,xi,xj,i,j,edges,type)
% return covariance matrix of anova kernel function
%

% set x
x=zeros(1,model.high_dim);
if (i>0),x(1,i)=xi;end    
if (j>0),x(1,j)=xj;end

% calculate covariance matrix according to the type
if strcmp(type,'auto')
    cov_matrix = model.cov_model(model.hyp, x, x,i,j,edges);
elseif strcmp(type,'cross')
    cov_matrix = model.cov_model(model.hyp, model.X, x,i,j,edges);
end

end


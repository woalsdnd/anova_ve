function y=levy(x)

d = length(x);

%rescale the domain to [-10,10]^*
bounds = [-10,10];
x(1:d) = bounds(1) + ...
    ((x(1:d)+1)/2)*(bounds(2) - bounds(1));

for ii = 1:d
	w(ii) = 1 + (x(ii) - 1)/4;
end

term1 = (sin(pi*w(1)))^2;
term3 = (w(d)-1)^2 * (1+(sin(2*pi*w(d)))^2);

sum = 0;
for ii = 1:(d-1)
	wi = w(ii);
        new = (wi-1)^2 * (1+10*(sin(pi*wi+1))^2);
	sum = sum + new;
end

y = term1 + sum + term3;

end




function y=crossit(x,Q)

%rotate the basis
x=x*Q;

%rescale the domain to [-10,10]X[-10,10]
bounds = [-10,10; -10,10];
x(1:2) = bounds(1:2, 1)' + ...
    (x(1:2)+1)/2.*(bounds(1:2, 2) - bounds(1:2, 1))';

x1 = x(1);
x2 = x(2);

fact1 = sin(x1)*sin(x2);
fact2 = exp(abs(100 - sqrt(x1^2+x2^2)/pi));

y = -0.0001 * (abs(fact1*fact2)+1)^0.1;

end




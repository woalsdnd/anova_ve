function y = branin(x,Q)
    %rotate the basis
    x=x*Q;

    %rescale the domain to [-5,10]X[0,15]
    bounds = [-5,10; 0,15];
    x(1:2) = bounds(1:2, 1)' + ...
        (x(1:2)+1)/2.*(bounds(1:2, 2) - bounds(1:2, 1))';
   
    %calculate the function
    y = (x(2)-(5.1/(4*pi^2))*x(1)^2+5*x(1)/pi-6)^2+10*(1-1/(8*pi))*cos(x(1))+10;
end
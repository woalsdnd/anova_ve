function y = logsumSE(x)
% return log sum of squared exponential 
% Eq : (1/(d-1)) * {sum -log (1+exp(-(1/2)*(x-mu1)K1^(-1)(x-mu1)')
%       +exp(-(1/2)*(x-mu2)K2^(-1)(x-mu2)')+8*exp(-(1/2)*(x-mu3)K3^(-1)(x-mu3)'))}
%
%

% set the dimension of maximum interaction
d=2;

% set the centers
c1=0.8*ones(1,d);
c1(1:2:end)=-0.3;
c2=[c1(2:end),c1(end-1)];
c3=-0.7*ones(1,d);

% set elements in the off-diagonal
el_tri=(1/75)*ones(d-1,1);
% set tridiagonal covariance matrix K1, K2, K3 
K1=(1/20)*eye(d)+diag(-1*el_tri,-1)+diag(-1*el_tri,1);
K2=(1/20)*eye(d)+diag(el_tri,-1)+diag(el_tri,1);
K3=(1/20)*eye(d);

% calculate the function
y=0;
for i=1:length(x)-1
    % xii : variables of (i)th and (i+1)th dimension
    xii=[x(i),x(i+1)];
    
    % (xii-c1)K1^(-1)(xii-c1)'
    L=chol(K1,'lower');
    z1=xii-c1;
    intermediate=L'\(L\z1');
    square_sum1=z1*intermediate;

    % (xii-c2)K2^(-1)(xii-c2)'
    L=chol(K2,'lower');
    z2=xii-c2;
    intermediate=L'\(L\z2');
    square_sum2=z2*intermediate;

    % (xii-c3)K3^(-1)(xii-c3)'
    L=chol(K3,'lower');
    z3=xii-c3;
    intermediate=L'\(L\z3');
    square_sum3=z3*intermediate;
  
    y = y-log(1+exp(-(1/2)*square_sum1)+exp(-(1/2)*square_sum2)+8*exp(-(1/2)*square_sum3));
end

y=y/(length(x)-1);

end
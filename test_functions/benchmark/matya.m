function y=matya(x,Q)

%rotate the basis
x=x*Q;

%rescale the domain to [-10,10]X[-10,10]
bounds = [-10,10; -10,10];
x(1:2) = bounds(1:2, 1)' + ...
    (x(1:2)+1)/2.*(bounds(1:2, 2) - bounds(1:2, 1))';

x1 = x(1);
x2 = x(2);

term1 = 0.26 * (x1^2 + x2^2);
term2 = -0.48*x1*x2;

y = term1 + term2;

end




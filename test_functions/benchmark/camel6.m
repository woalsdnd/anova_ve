function y=camel6(x,Q)

%rotate the basis
x=x*Q;

%rescale the domain to [-3,3]X[-2,2]
bounds = [-3,3; -2,2];
x(1:2) = bounds(1:2, 1)' + ...
    (x(1:2)+1)/2.*(bounds(1:2, 2) - bounds(1:2, 1))';


x1 = x(1);
x2 = x(2);

term1 = (4-2.1*x1^2+(x1^4)/3) * x1^2;
term2 = x1*x2;
term3 = (-4+4*x2^2) * x2^2;

y = term1 + term2 + term3;

end




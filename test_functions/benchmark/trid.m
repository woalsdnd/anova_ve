function y=trid(x,Q)
%rotate the basis
    x=x*Q;

d = length(x);

%rescale the domain to [-6^d,6^d]^*
bounds = [-6^d,6^d];
x(1:d) = bounds(1) + ...
    ((x(1:d)+1)/2)*(bounds(2) - bounds(1));

sum1 = (x(1)-1)^2;
sum2 = 0;

for ii = 2:d
	xi = x(ii);
	xold = x(ii-1);
	sum1 = sum1 + (xi-1)^2;
	sum2 = sum2 + xi*xold;
end

y = sum1 - sum2;

end




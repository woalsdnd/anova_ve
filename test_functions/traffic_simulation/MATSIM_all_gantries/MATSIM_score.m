function y=MATSIM_score(x)
    % return value shall be maximized

    d = length(x);

    %put values inside the range
    for i=1:d
       if x(i)>1,x(i)=1;end
       if x(i)<-1,x(i)=-1;end
    end
    
    % get the filename and current directory (which is used as home)
    this = mfilename;
    dir = which(this); dir = dir(1:end-2-numel(this));
    if exist('OCTAVE_VERSION') ~= 0 && numel(dir)==2
        if strcmp(dir,'./'), dir = [pwd,home_dir(2:end)]; end
    end
    if ~exist([dir 'TOLL'], 'dir'), mkdir([dir 'TOLL']);    end
    
    % set serial number = pid + month + date + time
    pid = num2str(feature('getpid'));
    sec=datevec(now);   curr_t=[];
    for i=2:5,  curr_t=[curr_t num2str(sec(i))];    end
    serial_num=[pid curr_t];
    variable_file=[dir 'TOLL/tolls_' serial_num];
    file=fopen(variable_file,'w');
    fprintf(file,'%1.1f\n',3*(x+ones(1,length(x))));%ERP prices vary between $0 and $6
    tic;
    [status, result] = system(['python ' dir 'simulateToll_score.py ' , variable_file,' ', serial_num],'-echo');
    fprintf('Time elapsed : %d sec\n',toc);
    y=str2num(result);
end

%run_experiment(fun_name,n_runs,n_iterations,n_init_pts,high_dim,projection_dim,hyp_update_cycle,exp_num,plotIteration,mst)
% 
% plotIteration : 'plot' or any other strings (no plot)
%
% REQUIREMENT
% mod (n_init_pts,hyp_update_cycle)==0
% mod (n_init_pts,mst_update_cycle)==0
% (to build an initial model)
% mod (mst_update_cycle,hyp_update_cycle)==0
% (to update Maximum Spanning Tree everytime hypers are updated)
% mod (n_iterations,hyp_update_cycle)==hyp_update_cycle-1 
% (to avoid model update at the last iteration)
%

startup;

run_experiment_matsim('MATSIM_travel_time',1,299,100,10,2,50,10,'noplot','find');
run_experiment_matsim('MATSIM_travel_time',1,299,100,10,2,50,10,'noplot','no');

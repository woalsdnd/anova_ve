function hyperparameters = learn_hyperparameters(model)
%learn length scales by maximizing log likelihood
%                       (minimizing negative log likelihood)

f = @(x) -log_likelihood(x, model);

%set initial value as current hyperparameters
xmin=model.hyp(1:end-1);
opt = optimset('GradObj','off','Display','off','MaxIter', 10,'Algorithm', 'sqp','TolCon', 1e-15, 'TolFun', 1e-15);
[optimizer,likeihood]=fmincon(f,xmin,[],[],[],[],model.hyper_bound(1:end-1,1),model.hyper_bound(1:end-1,2),[],opt);
hyperparameters=[optimizer; model.hyp(end)];
   
exp(hyperparameters)

end


function [obj_fun,init_pts,init_fs,bounds_domain]=load_initial_setting(method,n_init_pts,bounds_inits,high_dim,projection_dim,original_obj_fun)
% INPUT : method
%           0:original BO
%           1:random basis
%           2:random projection
%           3:subspace_learning
%           4:additive model
%           5:anova dcop
%
%         number of initial points
%         original dimension
%         projected(reduced) dimension
%         original objective function
%
% OUTPUT : objection function (after projection), initial points,
%          initial function values, bounds (in the domain)
%

if method==1 || method==2  || method==3    %projection based methods
    %[-sqrt(d),sqrt(d)]^d for projection based models 
    %(empirical value used in Bayesian optimization in high dimensions via random embeddings)
    bounds_domain = sqrt(projection_dim)*ones(projection_dim, 2); 
    bounds_domain(:, 1) = -bounds_domain(:, 1);
    
    if method==1    %random basis
        [A,~]=qr(randn(high_dim, projection_dim),0);
    elseif method==2 %random embedding
        A = randn(high_dim, projection_dim);
    else             %subspace learning
        n_centers=max(projection_dim,ceil(n_init_pts/(high_dim+1)));
        % evaluate function (D+1)*n_centers for derivative calculation
        % CAVEAT : n_centers >= projection_dim (if rank of X is lower than
        % k, SVD leads to meaningless basis for insufficient rank
        [A,init_pts,init_fs]=learn_subspace(n_centers,projection_dim,high_dim,original_obj_fun);
    end
    
    %initialize the objective function w.r.t projected dimension
    obj_fun = @(x) original_obj_fun((A*x')'); 
    
    %evaluate points randomly for random projection methods
    if method~=3
        [init_pts,init_fs]=random_selection(obj_fun,n_init_pts,projection_dim,bounds_inits,bounds_domain);
    end
    
else   %non-projection based methods (original BO, additive model, anova dcop)
    %[-1,1]^D for original BO, additive, gp-anova
    bounds_domain = ones(high_dim, 2); 
    bounds_domain(:, 1) = -bounds_domain(:, 1);

    obj_fun=original_obj_fun;
    
    if isequal(original_obj_fun,@MATSIM_score)
        load([getenv('HOME_DIR') 'init_data/init_all_gantries.mat']);
        init_fs=p_fs;
    elseif isequal(original_obj_fun,@MATSIM_score_expressways)
        load([getenv('HOME_DIR') 'init_data/init_expressways.mat']);
        init_fs=p_fs;
    elseif isequal(original_obj_fun,@MATSIM_travel_time)
        load([getenv('HOME_DIR') 'init_data/init_all_gantries.mat']);
        init_fs=tt_fs;
    elseif isequal(original_obj_fun,@MATSIM_travel_time_expressways)
        load([getenv('HOME_DIR') 'init_data/init_expressways.mat']);
        init_fs=tt_fs;
    end
end

end

function save_results(fun_name,Y,opt_values,run,method,exp_num)
%save simple regret & average cumulative regret
%

%set name of the output file
out_dir_name=[getenv('HOME_DIR') 'results/' fun_name '/'];
if ~exist(out_dir_name, 'dir'), mkdir(out_dir_name);    end
out_file_name=[out_dir_name 'result' num2str(exp_num) '.mat'];

%load previously saved data
if exist(out_file_name, 'file') == 2,   load(out_file_name);    end

%retrieve optimum value of the function
optimum=get_optimum(fun_name);

%calculate simple regret and average cumulative regret
T=(0:length(opt_values)-1)';
simple_regret{run,method}=optimum-opt_values;
N=size(T,1);    acr=zeros(N,1); acr(2:end)=Y;
regret=optimum-acr; avr_cumulative_regret{run,method}=zeros(N,1);
for i=2:N
    avr_cumulative_regret{run,method}(i)=sum(regret(2:i))/T(i);
end
avr_cumulative_regret{run,method}(1)=avr_cumulative_regret{run,method}(2);

%save results of the experiment
if exist(out_file_name, 'file') == 2
    save(out_file_name,'simple_regret','avr_cumulative_regret','-append');
else
    save(out_file_name,'simple_regret','avr_cumulative_regret');
end


end


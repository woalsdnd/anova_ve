function run_experiment_matsim(fun_name,n_runs,n_iterations,n_init_pts,high_dim,projection_dim,hyp_update_cycle,exp_num,plotIteration,find_mst)
%% set objective function
if strcmp(fun_name,'MATSIM_score')
    objective_function=@MATSIM_score;
    high_dim=77;
elseif strcmp(fun_name,'MATSIM_travel_time')
    objective_function=@MATSIM_travel_time;
    high_dim=77;
elseif strcmp(fun_name,'MATSIM_score_expressways')
    objective_function=@MATSIM_score_expressways;
    high_dim=27;
elseif strcmp(fun_name,'MATSIM_travel_time_expressways')
    objective_function=@MATSIM_travel_time_expressways;
    high_dim=27;
else
    objective_function=analytic_fun(fun_name,high_dim);
end        

%% set experiment setting function handler
if strcmp(find_mst,'find')
    fh=str2func('find_mst');
else
    fh=str2func(['exp_set_' fun_name]);
end

%% run ANOVA_VE
method=5;
for run=1:n_runs
    % initialize a model with the experiment settings 
    model=fh(method,objective_function,n_init_pts,n_iterations,high_dim,projection_dim,hyp_update_cycle);

    if ~strcmp(find_mst,'find')
        % run Bayesian Optimization
        [X,Y,opt_pts,opt_values,model]=BayesianOptimization(model,plotIteration);

        % save the results
        save_results(fun_name,Y,opt_values,run,method,exp_num);
                
        % save function values
        save_f_values(fun_name,model.f,run,method,exp_num);
    end
end

end
